class RPNCalculator
  # TODO: your code goes here!
  # OPERATORS = [:+, :- ,:* , :/]
  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def plus
    perform_operations(:+)
  end

  def value
    @stack[-1]
  end

  def minus
    perform_operations(:-)
  end

  def divide
    perform_operations(:/)
  end

  def times
    perform_operations(:*)
  end

  def tokens(string)
    tokens = string.split
    tokens.map { |ch| operant?(ch) ? ch.to_sym : Integer(ch) }
  end

  def evaluate(string)
    tokens = tokens(string)
    tokens.each do |token|
      case token
      when Integer
        push(token)
      else
        perform_operations(token)
      end
    end
    value
  end

  private

  def operant?(ch)
    [:+, :-, :*, :/].include?(ch.to_sym)
  end

  def perform_operations(op_sym)

    raise "calculator is empty" if @stack.size < 2

    second = @stack.pop
    first = @stack.pop

    case op_sym
    when :+
      @stack << first + second
    when :-
      @stack << first - second
    when :*
      @stack << first * second
    when :/
      @stack << first / second.to_f
    else
      @stack << first
      @stack << second
      raise "No such operation: #{op_sym}"
    end
  end


end
